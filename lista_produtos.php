<?php require("header.php"); ?>


<div class="container mt-5">


    <div class="card">
        <div class="card-header">
            LISTA DE PRODUTOS
            <a href="cadatrar.php" class="btn btn-primary btn-sm float-right">Novo produto</a>
        </div>

        <div class="card-body p-0">
            <?php
                $pprodutos = $prod->listAll();
                if(isset($pprodutos)):
            ?>
            <table class="table table-hover vertical align-middle">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Valor</th>
                    <th>Descrição</th>
                    <th>Opções</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    foreach($pprodutos as $produto):
                        ?>
                        <tr>
                            <td>
                                <?php echo $produto['img'] ? "<img src='./imagens/".$produto['img']."' width='60px' height='60px'>" : ""?>
                            </td>
                            <td><?php echo $produto['nome'] ?></td>
                            <td><?php echo "R$".$produto['valor'] ?></td>
                            <td><?php echo $produto['descricao'] ?></td>
                            <td>
                                <a href="edita_produto.php?id=<?php echo $produto['id']?>">Editar |</a>
                                <a href="deleta_produto.php?id=<?php echo $produto['id']?>">Exluir</a>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                else:
                    echo "<h3 class='p-3'>Nehnum produto encontrado</h3>";
                endif
                ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

<?php require("footer.php"); ?>
<?php

require('conn.php');


class produtoDAO {

    private $conn;

    public function __construct()
    {
        global $conn;
        $this->conn = $conn;
    }

    // Método para adicionar um novo produto
    public function adicionar($params)
    {
        $nome = $params['nome'];
        $valor = (float) str_replace(',', '.', $params['valor']);
        $descricao = $params['descricao'];

        if(!empty($params['img']['name'])){

            $formato_img = explode('/', $params['img']['type']);
            $img_name = md5(time().$params['img']['name']).".$formato_img[1]";

            $sql = "INSERT INTO produto SET nome = :nome, valor = :valor, descricao = :descricao, img = :img ";
            $sql = $this->conn->prepare($sql);
            $sql->bindValue(':nome', $nome);
            $sql->bindValue(':valor', $valor);
            $sql->bindValue(':descricao', $descricao);
            $sql->bindValue(':img', $img_name);
            $sql->execute();

            move_uploaded_file($params['img']['tmp_name'], 'imagens/'.$img_name);

        } else {
            $sql = "INSERT INTO produto SET nome = :nome, valor = :valor, descricao = :descricao ";
            $sql = $this->conn->prepare($sql);
            $sql->bindValue(':nome', $nome);
            $sql->bindValue(':valor', $valor);
            $sql->bindValue(':descricao', $descricao);
            $sql->execute();
        }



        return $this->conn->lastInsertId() ? true : false;


    }

    // Lista todos os produtos diponíveis
    public function listAll()
    {
        $sql = "SELECT * FROM produto";
        $sql = $this->conn->query($sql);

        if($sql->rowCount() > 0) {
            $res = $sql->fetchAll();

            return $res;
        }

    }

    // Lista um produto específico
    public function listSigle($id)
    {

        if(is_numeric($id)){

            $sql = "SELECT * FROM produto WHERE id =".$id;
            $sql = $this->conn->query($sql);

            if($sql->rowCount() > 0){
                return $sql->fetch();
            } else {
                echo "Produto não encontrado";
            }

        }

    }

    // Deleta um produto
    public function detele($id)
    {

        $sql = "DELETE FROM produto WHERE id = $id";
        $this->conn->query($sql);

    }

    // Edita um produto
    public function edit($params)
    {

        $id = $params['id'];
        $nome = $params['nome'];
        $valor = $params['valor'];
        $descricao = $params['descricao'];


        if(!empty($params['img']['name'])){

            $formato_img = explode('/', $params['img']['type']);
            $img_name = md5(time().$params['img']['name']).".$formato_img[1]";

            $sql = "UPDATE produto SET nome= :nome, valor = :valor, descricao = :descricao, img = :img WHERE id = :id";
            $sql = $this->conn->prepare($sql);
            $sql->bindValue(':id', $id);
            $sql->bindValue(':nome', $nome);
            $sql->bindValue(':valor', $valor);
            $sql->bindValue(':descricao', $descricao);
            $sql->bindValue(':img', $img_name);
            $sql->execute();

            move_uploaded_file($params['img']['tmp_name'], 'imagens/'.$img_name);

        } else {
            $sql = "UPDATE produto SET nome= :nome, valor = :valor, descricao = :descricao WHERE id = :id";
            $sql = $this->conn->prepare($sql);
            $sql->bindValue(':id', $id);
            $sql->bindValue(':nome', $nome);
            $sql->bindValue(':valor', $valor);
            $sql->bindValue(':descricao', $descricao);
            $sql->execute();
        }

        return true;

    }

    // Método para deletar apenas a imagem do produto
    public function deletaImg($id)
    {
        $sql = "UPDATE produto SET img = null WHERE id = $id";
        $this->conn->query($sql);
    }
    
}
<?php
require("header.php");

$id = 0;

if(!empty($_GET['id']) && isset($_GET['id'])){
    $id = addslashes($_GET['id']);
}

if(!empty($_POST['nome']) && isset($_POST['nome'])){

    $prod_edit['id'] = $id;
    $prod_edit['nome'] = addslashes($_POST['nome']);
    $prod_edit['valor'] = addslashes($_POST['valor']);
    $prod_edit['descricao'] = addslashes($_POST['descricao']);
    $prod_edit['img'] = $_FILES['img_produto'];

   if($prod->edit($prod_edit)){
       header('Location: lista_produtos.php');
   }

}

$produto = $prod->listSigle($id);

?>

<div class="container">

    <div class="row">
        <div class="col-sm-9">
            <div class="card mt-5">
                <div class="card-header">
                    Editar produto
                </div>

                <div class="card-body">
                    <form method="post" class="form_edit" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="nome_prod">Nome</label>
                            <input type="text" name="nome" id="nome_prod" class="form-control form-control-sm" value="<?php echo $produto['nome'] ?>">
                        </div>

                        <div class="form-group">
                            <label for="valor_prod">Valor</label>
                            <input type="text" name="valor" id="valor_prod" class="form-control form-control-sm" value="<?php echo $produto['valor'] ?>">
                        </div>

                        <div class="form-group">
                            <label for="descricao_prod">Descrição</label>
                            <input type="text" name="descricao" id="descricao_prod" class="form-control form-control-sm" value="<?php echo $produto['descricao'] ?>">
                        </div>

                        <?php if(!isset($produto['img']) && empty($produto['img'])): ?>
                        <div class="custom-file mt-3 mb-3">
                            <input type="file" name="img_produto" class="custom-file-input" id="img_produto">
                            <label class="custom-file-label" for="img_produto">Selecione o arquivo</label>
                        </div>
                        <?php endif ?>

                        <div class="form-group">
                            <a href="lista_produtos.php" class="btn btn-danger ml-2 float-right">Voltar</a>
                            <button type="submit" class="btn btn-primary float-right">Editar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="card mt-5 text-center" style="width: 18rem;">
                <?php echo $produto['img'] ? "<img src='./imagens/".$produto['img']."' width='280px' height='250px'>" : "Nenhuma imagem"?>
                <div class="card-body">
                    <a href="deleta_produto.php?id=<?php echo $produto['id']?>&img_del=1" class="btn btn-danger">Excluir</a>
                </div>
            </div>
        </div>
    </div>

</div>

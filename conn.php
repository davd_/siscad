<?php

global $conn;

$db['db'] = 'mysql';
$db['name'] = 'bleez';
$db['pass'] = '';
$db['user'] = 'root';
$db['host'] = "localhost";


try {

    $conn = new PDO($db['db'].":dbname=".$db['name'].";".$db['host'], $db['user'], $db['pass']);

} catch (Exception $e) {
    echo "Erro ao tentar se conectar com o banco de dados". " | " . $e->getMessage();
}
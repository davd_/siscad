<?php
require('header.php');
require('footer.php');
?>


<div class="container">
    <div class="card mt-5">
        <div class="card-header">
            Bem vindo(a) ao Siscad
        </div>

        <div class="card-body">
            Escolha uma opção a seguir <br>
            <a href="cadatrar.php" class="btn btn-primary btn-sm mt-3">Novo produto</a>
            <a href="lista_produtos.php" class="btn btn-primary btn-sm mt-3">Consultar produtos</a>
        </div>
    </div>
</div>


<?php
require("header.php");

if(!empty($_POST['nome']) && isset($_POST['nome'])){
    $poduto['nome'] = addslashes($_POST['nome']);
    $poduto['valor'] = addslashes($_POST['valor']);
    $poduto['descricao'] = addslashes($_POST['descricao']);
    $poduto['img'] = $_FILES['img_produto'];

    if($prod->adicionar($poduto)){
        header('Location: lista_produtos.php');
    }

}

?>

    <div class="container">
        <div class="card mt-5">
            <div class="card-header">
                Editar produto
            </div>

            <div class="card-body">
                <form method="post" class="form_edit" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="nome_prod">Nome</label>
                        <input type="text" name="nome" id="nome_prod" class="form-control form-control-sm">
                    </div>

                    <div class="form-group">
                        <label for="valor_prod">Valor</label>
                        <input type="text" name="valor" id="valor_prod" class="form-control form-control-sm">
                    </div>

                    <div class="form-group">
                        <label for="descricao_prod">Descrição</label>
                        <input type="text" name="descricao" id="descricao_prod" class="form-control form-control-sm">
                    </div>

                    <div class="custom-file mt-3 mb-3">
                        <input type="file" name="img_produto" class="custom-file-input" id="img_produto">
                        <label class="custom-file-label" for="img_produto">Selecione o arquivo</label>
                    </div>

                    <div class="form-group">
                        <a href="lista_produtos.php" class="btn btn-danger ml-2 float-right">Voltar</a>
                        <button type="submit" class="btn btn-primary float-right">Cadastrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


<?php require("footer.php"); ?>